<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $guarded = ['id'];

    public function account(){
      return $this->belongsTo('App\Account');
    }
    public function payer(){
      return $this->belongsTo('App\Payer');
    }
    public function type(){
      return $this->belongsTo('App\Type');
    }
    public function state(){
      return $this->belongsTo('App\State');
    }
    public function report(){
      return $this->belongsTo('App\Report');
    }
}
