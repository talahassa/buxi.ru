<?php

use App\State;
use App\Charts\ReportChart;


// Construct Charts

if(!function_exists('makeChart')){
    function makeChart($transactions, $type, $states, $sum){
        $chart = new ReportChart;
        $labels = collect([]);
        $data = collect([]);
        $backgroundColor = collect([]);
        $percent = 0;
        foreach ($states as $state) {
          $counter = 0;
          $bg_color = '';
          $percent = 0;
          foreach($state->transactions as $transaction){
              if($transaction->type_id == $type){
                $counter += $transaction->sum;
                $bg_color = State::find($transaction->state_id)->color;             
                //$percent = round(($counter * 100) / $sum);
              }                  
          } 
          if($counter > 0){
            $labels->push($state->name); 
            $data->push($counter);
            $backgroundColor->push($bg_color);  
          }
        }
        $chart->labels($labels);
        $chart->dataset(trans('main.income'), 'pie', $data)->options([
          'backgroundColor' => $backgroundColor,
          'displayLegend' => false,
        ]);
        return $chart;
      }
}