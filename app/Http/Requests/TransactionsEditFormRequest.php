<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransactionsEditFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return 1;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'created_date' => 'required',
          'sum' => 'required',
          'type_id' => 'required',
          'state_id' => 'required',
          'account_id' => 'required',
          'payer_id' => 'required',
        ];
    }
}
