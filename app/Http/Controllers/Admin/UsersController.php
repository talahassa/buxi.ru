<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Spatie\Permission\Models\Role;
use App\Http\Requests\UserEditFormRequest;
use App\Http\Requests\UserFormRequest;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
  public function index()
  {
    $users = User::all();
    $roles = Role::all();
    return view('backend.users.index', compact('users', 'roles'));
  }

  public function profile($id){
    $user = User::whereId($id)->firstOrFail();
    $roles = Role::all();
    return view('backend.users.profile', compact('user', 'roles'));
  }

  public function create(){
    $roles = Role::all();
    return view('backend.users.create', compact('roles'));
  }
  public function store(UserFormRequest $request)
  {
    User::create([
      'name' => $request->get('name'),
      'email' => $request->get('email'),
      'avatar' => $request->get('avatar'),
      'password' => Hash::make($request->get('password')),
      'role' => $request->get('role'),    
    ]);
    return redirect('/admin/users/create')->with('status', 'A new user has been created!');
  }
  public function edit($id)
  {
      $user = User::whereId($id)->firstOrFail();
      $roles = Role::all();
      $selectedRoles = $user->roles()->pluck('name')->toArray();
      return view('backend.users.edit', compact('user', 'roles', 'selectedRoles'));
  }
  public function update($id, UserEditFormRequest $request)
  {
      $user = User::whereId($id)->firstOrFail();

      if($request->get('avatar')){
        $avatarName = $user->id.'_avatar'.time().'.'.request()->avatar->guessExtension();
        $request->avatar->storeAs('public/avatars', $avatarName);
        $user->avatar = $avatarName;
      }    

      $user->name = $request->get('name');
      $user->email = $request->get('email');
      $password = $request->get('password');
      if($password != "") {
          $user->password = Hash::make($password);
      }
      $user->save();

      $user->syncRoles($request->get('role'));

      return redirect(action('Admin\UsersController@edit', $user->id))->with('status', 'The user has been updated!');
  }
}
