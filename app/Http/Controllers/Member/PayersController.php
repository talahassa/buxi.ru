<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use App\Http\Requests\PayersFormRequest;
use App\Http\Requests\PayersEditFormRequest;
use App\Http\Controllers\Controller;
use App\Payer;

class PayersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $payers = Payer::all();
      return view('member.payers.index', compact('payers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $payers = Payer::all();
      return view('member.payers.index', compact('payers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(PayersFormRequest $request)
     {
       $payers = new Payer(array(
           'name' => $request->get('name')
       ));
       $payers->save();

       return redirect('/member/payers')->with('status', 'Плательщик создан!');

     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $payer = Payer::whereId($id)->firstOrFail();
      return view('member.payers.edit', compact('payer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, PayersEditFormRequest $request)
    {
      $payer = Payer::whereId($id)->firstOrFail();
      $payer->name = $request->get('name');
      $payer->save();

      return redirect(action('Member\PayersController@edit', $payer->id))->with('status', 'Плательщик "'.$payer->name.'" обновлён.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $payer = Payer::whereId($id)->firstOrFail();
      $payer->delete();

      return redirect(action('Member\PayersController@index', $payer->id))->with('status', 'Плательщик "'.$payer->name.'" удалён!');
    }
}
