<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use App\Http\Requests\StatesFormRequest;
use App\Http\Requests\StatesEditFormRequest;
use App\Http\Controllers\Controller;
use App\State;

class StatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $states = State::withTrashed()->get();
      return view('member.states.index', compact('states'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $states = State::all();
      return view('member.states.index', compact('states'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(StatesFormRequest $request)
     {
       $states = new State(array(
           'name' => $request->get('name'),
           'color' => $request->get('color')
       ));
       $states->save();

       return redirect('/member/states')->with('status', trans('states.created'));

     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $state = State::withTrashed()->whereId($id)->firstOrFail();
      return view('member.states.edit', compact('state'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, StatesEditFormRequest $request)
    {
      $state = State::whereId($id)->firstOrFail();
      $state->name = $request->get('name');
      $state->color = $request->get('color');
      $state->save();

      return redirect(action('Member\StatesController@edit', $state->id))->with('status', 'Статья "'.$state->name.'" обновлена.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $state = State::whereId($id)->firstOrFail();
      $state->delete();

      return redirect(action('Member\StatesController@index', $state->id))->with('status', 'Статья "'.$state->name.'" удалена!');
    }

    public function restore($id)
    {
      $state = State::withTrashed()->find($id)->restore();
      return redirect(action('Member\StatesController@index', $id))->with('status', 'Статья "'.$id.'" восстановлена!');
    }
}
