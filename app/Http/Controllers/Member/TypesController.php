<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use App\Http\Requests\TypesFormRequest;
use App\Http\Requests\TypesEditFormRequest;
use App\Http\Controllers\Controller;
use App\Type;

class TypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $types = Type::all();
      return view('member.types.index', compact('types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $types = Type::all();
      return view('member.types.index', compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(TypesFormRequest $request)
     {
       $types = new Type(array(
           'name' => $request->get('name')
       ));
       $types->save();

       return redirect('/member/types')->with('status', trans('types.created'));

     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $type = Type::whereId($id)->firstOrFail();
      return view('member.types.edit', compact('type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, TypesEditFormRequest $request)
    {
      $type = Type::whereId($id)->firstOrFail();
      $type->name = $request->get('name');
      $type->save();

      return redirect(action('Member\TypesController@edit', $type->id))->with('status', 'Тип транзакции "'.$type->name.'" создан.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $type = Type::whereId($id)->firstOrFail();
      $type->delete();

      return redirect(action('Member\TypesController@index', $type->id))->with('status', 'Тип транзакции "'.$type->name.'" удалена!');
    }
}
