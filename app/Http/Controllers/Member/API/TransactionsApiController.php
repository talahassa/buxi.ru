<?php

namespace App\Http\Controllers\Member\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transaction;
use App\Account;

class TransactionsApiController extends Controller
{
    function index(){
        $transactions = Transaction::with(['account', 'type', 'payer', 'state'])->orderBy('created_date', 'desc')->paginate(5);        
    	return response()->json($transactions);
    }

    public function getAccountId($id){
        $account = Account::withTrashed()->whereId($id)->firstOrFail();
        return response()->json($account);
    }
}
