<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use App\Http\Requests\AccountsFormRequest;
use App\Http\Requests\AccountEditFormRequest;
use App\Http\Controllers\Controller;
use App\Account;

class AccountsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $accounts = Account::withTrashed()->get();
      return view('member.accounts.index', compact('accounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $accounts = Account::all();
      return view('member.accounts.index', compact('accounts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(AccountsFormRequest $request)
     {
       $account = new Account(array(
           'name' => $request->get('name'),
           'balance' => $request->get('balance')
       ));
       $account->save();

       return redirect('/member/accounts')->with('status', 'Счёт создан!');

     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $account = Account::withTrashed()->whereId($id)->firstOrFail();
      return view('member.accounts.edit', compact('account'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, AccountEditFormRequest $request)
    {
      $account = Account::whereId($id)->firstOrFail();
      $account->name = $request->get('name');
      $account->balance = $request->get('balance');
      $account->save();

      return redirect(action('Member\AccountsController@index', $account->id))->with('status', 'Аккаунт "'.$account->name.'" обновлён.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $account = Account::whereId($id)->firstOrFail();
      $account->delete();

      return redirect(action('Member\AccountsController@index', $account->id))->with('status', 'Аккаунт "'.$account->name.'" удалён!');
    }
    public function restore($id)
    {
      $state = Account::withTrashed()->find($id)->restore();
      return redirect(action('Member\AccountsController@index', $id))->with('status', 'Аккаунт "'.$id.'" восстановлена!');
    }
}
