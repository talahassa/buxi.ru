<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use App\Http\Requests\TransactionsFormRequest;
use App\Http\Requests\TransactionsEditFormRequest;
use App\Http\Controllers\Controller;
use App\Account;
use App\Transaction;
use App\Payer;
use App\Type;
use App\State;
use Carbon\Carbon;


class TransactionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      // Same but not paginated for Statistics
      $transactions_month_paginated = Transaction::whereMonth('created_date', Carbon::today()->format('m'))->whereYear('created_date', Carbon::today()->format('Y'))->get();
      // Get All Transactions with Paginations for the main transaction page
      $transactions = Transaction::orderBy('created_date', 'desc')->simplePaginate(55);
      $accounts = Account::all();
      $payers = Payer::all();
      $payers_for_month = Payer::with(['transactions' => function($query) {
        $query->whereMonth('created_date', Carbon::today()->format('m'))->whereYear('created_date', Carbon::today()->format('Y'));
      }])->get();
      $states = State::all();
      $states_for_month = State::with(['transactions' => function($query) {
        $query->whereMonth('created_date', Carbon::today()->format('m'))->whereYear('created_date', Carbon::today()->format('Y'));
      }])->get();
      $types = Type::all();

      $dohods = $rashods = $rashod_sum = $dohod_sum = $income = $invested = $didek_sum = $nargiza_sum = $all_sum = $didek_min = $nargiza_min = $all_min = $compulsory_rashods = $compulsory_rashod_sum = $underinvested = $invest_class = $total_balance = $both_sum = $both_minus = $both_invest = 0;


      ///$transactions_month_paginated->where('type_id', 2)->where('payer_id', $payer->id)->sum('sum');
      foreach($transactions_month_paginated as $transaction){
        if($transaction->type->id == 1){
          $rashods++;
          $rashod_sum += $transaction->sum;
          if($transaction->compulsory != 1){
            $compulsory_rashods++;
            $compulsory_rashod_sum += $transaction->sum;
          }
        } elseif($transaction->type->id == 2){
          $dohods++;
          $dohod_sum += $transaction->sum;          
        } else{
          if($transaction->state->id == 2){
            $invested += $transaction->sum;
          }
        }
      }

      $income = $dohod_sum - $rashod_sum;

      if(($dohod_sum * 10)/100 > $invested){
        $invest_class = 'blink-danger';
        $underinvested = ($dohod_sum * 10)/100 - $invested;
      } else{
        $invest_class = 'bg-success text-white ';
      }

      $total_balance = $accounts->sum('balance');

      $output_payer= array();

      foreach($payers_for_month as $payer){

        if($payer->id != 3){  

          $output_payer[$payer->name]['rashod'] = $transactions_month_paginated->where('type_id', 1)->where('payer_id', $payer->id)->sum('sum') + ( $transactions_month_paginated->where('type_id', 1)->where('payer_id', 3)->sum('sum') / 2);

          $output_payer[$payer->name]['dohod'] = $transactions_month_paginated->where('type_id', 2)->where('payer_id', $payer->id)->sum('sum') + ($transactions_month_paginated->where('type_id', 2)->where('payer_id', 3)->sum('sum') / 2 );

          $output_payer[$payer->name]['profit'] = $transactions_month_paginated->where('type_id', 2)->where('payer_id', $payer->id)->sum('sum') -  $transactions_month_paginated->where('type_id', 1)->where('payer_id', $payer->id)->sum('sum');
          
          $output_payer[$payer->name]['invested'] = $transactions_month_paginated->where('payer_id', $payer->id)->where('type_id', 3)->where('state_id', 2)->sum('sum'); 

         } else {
           
          $output_payer[$payer->name]['rashod'] = $transactions_month_paginated->where('type_id', 1)->sum('sum');

          $output_payer[$payer->name]['dohod'] = $transactions_month_paginated->where('type_id', 2)->sum('sum');

          $output_payer[$payer->name]['profit'] = $transactions_month_paginated->where('type_id', 2)->sum('sum') - $transactions_month_paginated->where('type_id', 1)->sum('sum');

          $output_payer[$payer->name]['invested'] = $transactions_month_paginated->where('type_id', 3)->where('state_id', 2)->sum('sum');
            
          } 
      }   
      
      //print_r($output_payer);

      return view('member.transactions.index', compact('transactions', 'transactions_month', 'states', 'types', 'accounts', 'payers', 'account', 'payers_stats', 'transactions_month_paginated', 'payers_for_month', 'rashods', 'rashod_sum', 'compulsory_rashods', 'compulsory_rashod_sum', 'dohods', 'dohod_sum', 'invested', 'income', 'invest_class', 'underinvested', 'total_balance', 'both_sum', 'both_minus', 'both_invest', 'output_payer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $transactions = Transaction::all();
      return view('member.transactions.index', compact('transactions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(TransactionsFormRequest $request)
     {
       $transaction = new Transaction(array(
           'created_date' => Carbon::parse($request->get('created_date'))->format('Y-m-d H:i:s'),
           'sum' => $request->get('sum'),
           'type_id' => $request->get('type_id'),
           'state_id' => $request->get('state_id'),
           'description' => $request->get('description'),
           'account_id' => $request->get('account_id'),
           'account_to_id' => $request->get('account_to_id'),
           'payer_id' => $request->get('payer_id'),
           'compulsory' => $request->get('compulsory'),
       ));
       // Saving logic

       $account = Account::find($request->get('account_id'));
       if($transaction->type_id == 1){
         $intersumm = floatval($account->balance) - floatval($request->get('sum'));
         Account::whereId($request->get('account_id'))->update(array('balance' => $intersumm));
       }
       if($transaction->type_id == 2){
         $intersumm = floatval($account->balance) + floatval($request->get('sum'));
         Account::whereId($request->get('account_id'))->update(array('balance' => $intersumm));
       }
       if($transaction->type_id == 3){
         $account_to = Account::find($request->get('account_to_id'));
         $summ_minus = floatval($account->balance) - floatval($request->get('sum'));
         $summ_plus = floatval($account_to->balance) + floatval($request->get('sum'));
         Account::whereId($request->get('account_id'))->update(array('balance' => $summ_minus));
         Account::whereId($request->get('account_to_id'))->update(array('balance' => $summ_plus));
       }
       // Save to DB
       $transaction->save();
       // Redirect to view
       return redirect('/member/transactions')->with('status', trans('transactions.created'));

     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $transaction = Transaction::whereId($id)->firstOrFail();
      $accounts = Account::all();
      $payers = Payer::all();
      $states = State::all();
      $types = Type::all();
      $original_account = $accounts->find($transaction->getOriginal('account_id'))->name;
      $original_type = $types->find($transaction->getOriginal('type_id'))->name;
      $original_sum = $transaction->getOriginal('sum');
      $original = $transaction->getOriginal();
      $original_balance = $accounts->find($transaction->getOriginal('account_id'))->balance;
      return view('member.transactions.edit', compact('transaction', 'states', 'types', 'accounts', 'payers', 'original_account', 'original_sum', 'original', 'original_balance', 'original_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, TransactionsEditFormRequest $request)
    {
      $transaction = Transaction::whereId($id)->firstOrFail();
      $transaction->created_date = Carbon::parse($request->get('created_date'))->format('Y-m-d H:i:s');
      $transaction->sum = $request->get('sum');
      $transaction->type_id = $request->get('type_id');
      $transaction->state_id = $request->get('state_id');
      $transaction->description = $request->get('description');
      $transaction->account_id = $request->get('account_id');
      $transaction->account_to_id = $request->get('account_to_id');
      $transaction->payer_id = $request->get('payer_id');
      $transaction->compulsory = $request->get('compulsory');
      $account = Account::find($transaction->account_id);
      $account_to = Account::find($transaction->account_to_id);
      $account_original = Account::find($transaction->getOriginal('account_id'));

      
      // Тип отличается
      if($transaction->getOriginal('type_id') != $transaction->type_id)
      {
        // Аккаунты меняются: 
        if($transaction->getOriginal('account_id') != $transaction->account_id)        
        { //Расход, меняется на Доход
          if($transaction->getOriginal('type_id') == 1 && $transaction->type_id == 2){

            $return_sum_to_balance = floatval($account_original->getOriginal('balance')) + floatval($transaction->getOriginal('sum'));
  
            // Return Money back to the Original Balance
            $account_original->update(array('balance' => $return_sum_to_balance));   

            $new_sum_to_balance = floatval($account->balance) + floatval($transaction->sum);
            
            // Update Balance with new Sum
            $account->update(array('balance' => $new_sum_to_balance));
  
          } elseif($transaction->getOriginal('type_id') == 2 && $transaction->type_id == 1){
  
            $return_sum_to_balance = floatval($account_original->getOriginal('balance')) - floatval($transaction->getOriginal('sum')); 
            // Return Money back to the Original Balance
            $account_original->update(array('balance' => $return_sum_to_balance));

            $new_sum_to_balance = floatval($account->balance) - floatval($transaction->sum);
            
            // Update Balance with new Sum
            $account->update(array('balance' => $new_sum_to_balance));
            
          }
        // Оригинальные аккаунты не меняются
        }else{
          //Расход меняется на Доход
          if($transaction->getOriginal('type_id') == 1 && $transaction->type_id == 2){

            $return_sum_to_balance = floatval($account->balance) + floatval($transaction->getOriginal('sum'));
  
            // Return Money back to the Original Balance
            $account->update(array('balance' => $return_sum_to_balance));   

            $new_sum_to_balance = floatval($account->balance) + floatval($transaction->sum);
            
            // Update Balance with new Sum
            $account->update(array('balance' => $new_sum_to_balance));
  
          } //Доход меняется на Расход
          elseif($transaction->getOriginal('type_id') == 2 && $transaction->type_id == 1){

            $return_sum_to_balance = floatval($account->balance) - floatval($transaction->getOriginal('sum'));
  
            // Return Money back to the Original Balance
            $account->update(array('balance' => $return_sum_to_balance));   

            $new_sum_to_balance = floatval($account->balance) - floatval($transaction->sum);
            
            // Update Balance with new Sum
            $account->update(array('balance' => $new_sum_to_balance));  
          } 
          //Доход меняется на Перевод
          elseif($transaction->getOriginal('type_id') == 2 && $transaction->type_id == 3){

            $return_sum_to_balance = floatval($account->balance) - floatval($transaction->getOriginal('sum'));
  
            // Return Money back to the Original Balance
            $account->update(array('balance' => $return_sum_to_balance));   

            $new_sum_to_balance = floatval($account_to->balance) - floatval($transaction->sum);
            
            // Update Balance with new Sum
            $account_to->update(array('balance' => $new_sum_to_balance));  
          }
        }        
      } else{        
        // Tип транзакции не меняется
        // Отличается Аккаунт
          if($transaction->account_id != $transaction->getOriginal('account_id')){
            if($transaction->getOriginal('type_id') == 1){
              $return_sum_to_original_balance = floatval($account_original->balance) + floatval($transaction->getOriginal('sum'));
  
              // Return Money back to the Original Balance
              $account_original->update(array('balance' => $return_sum_to_original_balance));   
  
              $new_sum_to_balance = floatval($account->balance) - floatval($transaction->sum);
              
              // Update Balance with new Sum
              $account->update(array('balance' => $new_sum_to_balance));
            }  
            if($transaction->getOriginal('type_id') == 2){
              $return_sum_to_original_balance = floatval($account_original->balance) - floatval($transaction->getOriginal('sum'));
  
              // Return Money back to the Original Balance
              $account_original->update(array('balance' => $return_sum_to_original_balance));   
  
              $new_sum_to_balance = floatval($account->balance) + floatval($transaction->sum);
              
              // Update Balance with new Sum
              $account->update(array('balance' => $new_sum_to_balance));
            }            
          }
      }
      if($transaction->getOriginal('type_id') == $transaction->type_id && $transaction->account_id == $transaction->getOriginal('account_id')){
        // Tип транзакции не меняется
        // Отличается сумма
        if($transaction->getOriginal('sum') != $transaction->sum)
        {
          //Расход меняется на Доход
          if($transaction->getOriginal('type_id') == 1){

            $return_sum_to_balance = floatval($account->balance) + floatval($transaction->getOriginal('sum'));

            // Return Money back to the Original Balance
            $account->update(array('balance' => $return_sum_to_balance));   

            $new_sum_to_balance = floatval($account->balance) - floatval($transaction->sum);
            
            // Update Balance with new Sum
            $account->update(array('balance' => $new_sum_to_balance));
          }
          //Доход меняется на Расход
          if($transaction->getOriginal('type_id') == 2){

            $return_sum_to_balance = floatval($account->balance) - floatval($transaction->getOriginal('sum'));

            // Return Money back to the Original Balance
            $account->update(array('balance' => $return_sum_to_balance));   

            $new_sum_to_balance = floatval($account->balance) + floatval($transaction->sum);
            
            // Update Balance with new Sum
            $account->update(array('balance' => $new_sum_to_balance));
          }
        }
      }
      
      
      $transaction->save();

      return redirect(action('Member\TransactionsController@edit', $transaction->id))->with('status', trans('transactions.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $transaction = Transaction::whereId($id)->firstOrFail();
      $account = Account::find($transaction->account_id);
      $account_original = Account::find($transaction->getOriginal('account_id'));

      if($transaction->getOriginal('type_id') == 1){
        $return_sum_to_original_balance = floatval($account_original->getOriginal('balance')) + floatval($transaction->getOriginal('sum'));  
        // Return Money back to the Original Balance
        $account_original->update(array('balance' => $return_sum_to_original_balance)); 
      }
      if($transaction->type_id == 2){
        $return_sum_to_original_balance = floatval($account_original->getOriginal('balance')) - floatval($transaction->getOriginal('sum'));  
        // Return Money back to the Original Balance
        $account_original->update(array('balance' => $return_sum_to_original_balance)); 
      }
      if($transaction->type_id == 3){
        $account_to = Account::find($transaction->getOriginal('account_to_id'));
        $summ_minus = floatval($account_original->getOriginal('balance')) + floatval($transaction->getOriginal('sum'));
        $summ_plus = floatval($account_to->getOriginal('balance')) - floatval($transaction->getOriginal('sum'));
        $account_original->update(array('balance' => $summ_minus));
        $account_to->update(array('balance' => $summ_plus));
      }
      $transaction->delete();

      return redirect(action('Member\TransactionsController@index'))->with('status', trans('transactions.deleted'));
    }
}