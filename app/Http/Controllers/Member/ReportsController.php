<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ReportsFormRequest;
use App\Http\Requests\ReportsEditFormRequest;
use App\Http\Controllers\Controller;
use App\Report;
use App\Account;
use App\Transaction;
use App\Payer;
use App\Type;
use App\State;
use Carbon\Carbon;
use App\Charts\ReportChart;

class ReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $reports = Report::all();
        return view('member.reports.index', compact('reports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReportsFormRequest $request)
    {
      // Validate form
      // set form validation rules

       $this->validate($request, [
           'start_date' => 'required|date_format:d.m.Y',
           'end_date' => 'required|date_format:d.m.Y',
       ]);

       $report = new Report(array(
           'start_date' =>  Carbon::parse($request->get('start_date'))->format('Y-m-d H:i:s'),
           'end_date' => Carbon::parse($request->get('end_date'))->format('Y-m-d H:i:s'),
           'title' => $request->get('title'),
       ));
       $report->save();

       //return response()->json(['success'=>'Done!']);
       return redirect('/member/reports')->with('status', 'Report created');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      // Get the Report
      $report = Report::whereId($id)->firstOrFail();
      $transactions = Transaction::whereBetween(DB::raw('date(created_date)'), [$report->start_date, $report->end_date])->get();
      $accounts = Account::all();
      $payers = Payer::with(['transactions' => function($query) use ($report){
        $query->whereBetween(DB::raw('date(created_date)'), [$report->start_date, $report->end_date]);
      }])->get();
      $states = State::with(['transactions' => function($query) use ($report){
        $query->whereBetween(DB::raw('date(created_date)'), [$report->start_date, $report->end_date]);
      }])->get();
      $types = Type::all();
      return view('member.reports.show', compact('transactions', 'report', 'states', 'types', 'accounts', 'payers'));
    }

    public function thisMonth()
    {
      $transactions = Transaction::whereMonth('created_date', Carbon::today()->format('m'))->get();
      $accounts = Account::all();
      $payers = Payer::with(['transactions' => function($query) {
        $query->whereMonth('created_date', Carbon::today()->format('m'));
      }])->get();
      $states = State::with(['transactions' => function($query) {
        $query->whereMonth('created_date', Carbon::today()->format('m'));
      }])->get();
      $types = Type::all();
      return view('member.reports.thismonth', compact('transactions', 'states', 'types', 'accounts', 'payers'));
    }

    

    public function previousMonth()
    {
      $transactions = Transaction::whereMonth('created_date', Carbon::now()->subMonth()->month)->get();
      $dohod_sum = Transaction::whereMonth('created_date', Carbon::now()->subMonth()->month)->where('type_id', 2)->sum('sum');
      $rashod_sum = Transaction::whereMonth('created_date', Carbon::now()->subMonth()->month)->where('type_id', 1)->sum('sum');

      $accounts = Account::all();
      $payers = Payer::with(['transactions' => function($query) {
        $query->whereMonth('created_date', Carbon::now()->subMonth()->month);
      }])->get();
      $states = State::with(['transactions' => function($query) {
        $query->whereMonth('created_date', Carbon::now()->subMonth()->month);
      }])->get();
      $types = Type::all();


    /*      
    // Charts
    */
    $rashod_chart = makeChart($transactions, 1, $states, $rashod_sum);
    $dohod_chart = makeChart($transactions, 2, $states, $dohod_sum);
      

      return view('member.reports.previous_month', compact('transactions', 'states', 'types', 'accounts', 'payers', 'dohod_chart', 'rashod_chart', 'dohod_sum', 'rashod_sum'));
    }

    public function thisYear()
    {
      $transactions = Transaction::whereYear('created_date', Carbon::now()->format('Y'))->get();
      $accounts = Account::all();
      $payers = Payer::with(['transactions' => function($query) {
        $query->whereYear('created_date', Carbon::now()->format('Y'));
      }])->get();
      $states = State::with(['transactions' => function($query) {
        $query->whereYear('created_date', Carbon::now()->format('Y'));
      }])->get();
      $types = Type::all();
      return view('member.reports.this_year', compact('transactions', 'states', 'types', 'accounts', 'payers'));
    }

    public function transactionsByFilter(Request $request)
    {   
        $query = Transaction::orderBy('created_date', 'desc')->with(['Type', 'State', 'Account', 'Payer']);

      if($request->has('date')){
        $query->whereDate('created_date', Carbon::parse($request->date)->format('Y-m-d'));
      }
      if($request->has('type')){
        $query->whereHas('Type', function($q) use($request){ $q->where('id', $request->type);})->simplePaginate(55);
      }
      if($request->has('state')){
        $query->whereHas('State', function($q) use($request){ $q->where('id', $request->state);})->simplePaginate(55);
      }
      if($request->has('account')){
        $query->whereHas('Account', function($q) use($request){ $q->where('id', $request->account);})->simplePaginate(55);
      }
      if($request->has('payer')){
        $query->whereHas('Payer', function($q) use($request){ $q->where('id', $request->payer);})->simplePaginate(55);
      }

      $transactions = $query->simplePaginate(55);
      $transactions->appends(['state' => $request->get('state')]);
      $transactions->appends(['date' => $request->get('date')]);
      $transactions->appends(['type' => $request->get('type')]);
      $transactions->appends(['account' => $request->get('account')]);
      $transactions->appends(['payer' => $request->get('payer')]);
      
      $accounts = Account::all();
      $payers = Payer::all();
      $states = State::all();
      $types = Type::all();
      return view('member.reports.bytype', compact('transactions', 'states', 'types', 'accounts', 'payers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function edit(Report $report)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Report $report)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $report = Report::whereId($id)->firstOrFail();
      $report->delete();

      return redirect(action('Member\ReportsController@index', $report->id))->with('status', 'Отчёт №"'.$report->id.'" удален');
    }
}
