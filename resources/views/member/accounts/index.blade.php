@extends('layouts.app')
@section('title', trans('accounts.title'))
@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-8 mt-2">
      <div class="card">
        <div class="card-header ">
          <h5 class="float-left m-0">{{ trans('accounts.title') }}</h5>
        </div>
        <div class="card-body mt-2">
          @if (session('status'))
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
          @endif
            @if ($accounts->isEmpty())
              <p>{{ trans('accounts.nodata') }}</p>
            @else
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th scope="col">{{ trans('accounts.title_name') }}</th>
                    <th scope="col">{{ trans('accounts.balance') }}</th>
                  </tr>
                </thead>
                <tbody>
                @foreach($accounts as $account)
                  <tr>
                    <td>
                      <a href="{{ action('Member\AccountsController@edit', $account->id) }}">
                        @if($account->trashed()) <strike> @endif
                          {{ $account->name }}
                        @if($account->trashed()) </strike> @endif
                        </a>
                    </td>
                    <td>
                      {{ $account->balance }}
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            @endif
        </div>
      </div>
    </div>
    <div class="col-md-4 mt-2">
        <div class="card">
            <div class="card-header ">
                <h5 class="float-left m-0">Добавить Счёт</h5>
            </div>
            <div class="card-body mt-2">
              @foreach ($errors->all() as $error)
                  <p class="alert alert-danger">{{ $error }}</p>
              @endforeach
              {!! Form::open(['class' => 'form', 'method' => 'post', 'action' => 'Member\AccountsController@store']) !!}
              <div class="form-group">
                <div class="col-lg-12">
              {!! Form::text('name', null, [
                'class' => 'form-control',
                'id' => 'name',
                'placeholder' => 'Название'
                ])
              !!}
              </div>
              </div>
              <div class="form-group">
                <div class="col-lg-12">
              {!! Form::text('balance', 0, [
                'class' => 'form-control',
                'id' => 'balance',
                'placeholder' => 'Баланс'
                ])
              !!}
              </div>
              </div>
              <div class="col-lg-12">
              <div class="form-group">
              {!! Form::submit('Добавить', [
                'class' => 'btn btn-primary'
                ])
              !!}
              </div>
              </div>

              {!! Form::close() !!}
            </div>
        </div>
    </div>
  </div>
</div>
@endsection
