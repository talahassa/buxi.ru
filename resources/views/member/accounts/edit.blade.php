@extends('layouts.app')
@section('title', 'Edit an account')

@section('content')
    <div class="container col-md-8 col-md-offset-2">
        <div class="card mt-5">
            <div class="card-header ">
                <h5 class="float-left">Edit an account</h5>
            </div>
            <div class="card-body mt-2">
              @foreach ($errors->all() as $error)
                  <p class="alert alert-danger">{{ $error }}</p>
              @endforeach

              @if (session('status'))
                  <div class="alert alert-success">
                      {{ session('status') }}
                  </div>
              @endif
              {!! Form::open(['class' => 'form']) !!}
              <div class="form-row">
              <div class="form-group col">
                <label for="name" class="col-sm-6 col-form-label">Название</label>
                <div class="col-lg-12">
              {!! Form::text('name', $account->name, [
                'class' => 'form-control',
                'id' => 'name',
                'placeholder' => 'Название'
                ])
              !!}
              </div>
              </div>
              <div class="form-group col">
                <label for="name" class="col-sm-6 col-form-label">Баланс</label>
                <div class="col-lg-12">
              {!! Form::text('balance', $account->balance, [
                'class' => 'form-control',
                'id' => 'balance',
                ])
              !!}
              </div>
              </div>
              </div>
              <div class="col-lg-12">
              <div class="form-group">
              {!! Form::submit('Обновить', [
                'class' => 'btn btn-primary'
                ])
              !!}
              </div>
              </div>

              {!! Form::close() !!}

              {!! Form::open(['class' => 'form', 'method' => 'post', 'action' => ['Member\AccountsController@destroy', $account->id ]]) !!}
              <div class="col-lg-12">
              <div class="form-group">
              {!! Form::submit('Удалить?', [
                'class' => 'btn btn-danger',
                ])
              !!}
              </div>
              </div>
              {!! Form::close() !!}
              @if($account->trashed())
              {!! Form::open(['class' => 'form', 'method' => 'post', 'action' => ['Member\AccountsController@restore', $account->id ]]) !!}
              <div class="col-lg-12">
              <div class="form-group">
              {!! Form::submit(trans('main.restore'), [
                'class' => 'btn btn-success',
                ])
              !!}
              </div>
              </div>
              {!! Form::close() !!}
              @endif
            </div>
        </div>
    </div>
@endsection
