@extends('layouts.app')
@section('title', trans('transactions.edit'))

@section('content')
Original account id: {{$original_account}}<br/>
Original type id: {{$original_type}}<br/>
Original balance SUM: {{$original_sum}}
    <div class="container col-md-8 col-md-offset-2">
        <div class="card mt-5">
            <div class="card-header ">
                <h5 class="float-left mb-0">{{ trans('transactions.edit') }}</h5>
            </div>
            <div class="card-body mt-2">
              @foreach ($errors->all() as $error)
                  <p class="alert alert-danger">{{ $error }}</p>
              @endforeach

              @if (session('status'))
                  <div class="alert alert-success">
                      {{ session('status') }}
                  </div>
              @endif
              {!! Form::open(['class' => 'form']) !!}
                <div class="form-group">
                  <div class="col-lg-12">
              {!! Form::text('created_date', \Carbon\Carbon::parse($transaction->created_date)->format('d.m.Y H:i:s'), [
                'class' => 'form-control',
                ])
              !!}
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-12">
              {!! Form::text('sum', $transaction->sum, [
                'placeholder' => trans('transactions.sum'),
                'class' => 'form-control',
                'id' => 'sum'
                ])
              !!}
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-12">
              {!! Form::select('type_id', App\Type::orderBy('name','asc')->pluck('name','id'), $types->find($transaction->type_id)->id, [
                'class' => 'form-control',
                'id' => 'type_id',
                ])
              !!}
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-12">
              {!! Form::select('state_id', App\State::orderBy('name','asc')->pluck('name','id'), $states->find($transaction->state_id)->id, [
                'class' => 'form-control',
                'id' => 'state_id',
                ])
              !!}
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-12">
              {!! Form::text('description', $transaction->description, [
                'class' => 'form-control',
                'id' => 'description',
                'placeholder' => trans('transactions.description')
                ])
              !!}
              </div>
              </div>
              <div class="form-group">
                <div class="col-lg-12">
              {!! Form::select('account_id', App\Account::orderBy('name','asc')->pluck('name','id'), $accounts->find($transaction->account_id)->id, [
                'class' => 'form-control',
                'id' => 'account_id',
              ])
              !!}
                </div>
              </div>
              @if($transaction->type_id == 3)
                <div class="form-group">
                  <div class="col-lg-12">
                {!! Form::select('account_to_id', App\Account::orderBy('name','asc')->pluck('name','id'), $accounts->find($transaction->account_to_id)->id, [
                  'class' => 'form-control',
                  'id' => 'account_to_id',
                ])
                !!}
                  </div>
                </div>
              @endif
              <div class="form-group">
                <div class="col-lg-12">
              {!! Form::select('payer_id', App\Payer::orderBy('name','asc')->pluck('name','id'), $payers->find($transaction->payer_id)->id, [
                'class' => 'form-control',
                'id' => 'payer_id',
                ])
              !!}
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-12">
                  <span class="switch">
                    {!! Form::checkbox('compulsory', '1', $transaction->compulsory, [
                      'id' => 'compulsoryCheck',
                      'class' => 'switch'
                      ]); !!}
                    <label class="switch-normal" for="compulsoryCheck">
                      Обязательно?
                    </label>
                  </span>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-12">
                {!! Form::submit(trans('main.update'), [
                  'class' => 'btn btn-primary'
                  ])
                !!}
                </div>
              </div>
            {!! Form::close() !!}
            {!! Form::open(['class' => 'form', 'method' => 'post', 'action' => ['Member\TransactionsController@destroy', $transaction->id ]]) !!}
            <div class="col-lg-12">
            <div class="form-group">
            {!! Form::submit(trans('main.delete'), [
              'class' => 'btn btn-danger',
              ])
            !!}
            </div>
            </div>

            {!! Form::close() !!}
              </div>
              </div>
            </div>
        </div>
    </div>
@endsection
