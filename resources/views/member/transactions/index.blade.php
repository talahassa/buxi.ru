@extends('layouts.app')
@section('title', trans('transactions.title'))
@section('statistics')
<div class="d-flex">
  <div class="flex-fill rounded-left bg-success text-white py-1 px-2 border-right border-white text-center" data-container="body" data-toggle="popover" data-placement="bottom" data-content="
  <table class='table table-bordered table-sm'>
    <thead class='table-success'>
    <tr>
      <th scope='col' colspan='2'>Доходы</th>
    </tr>
  </thead>
  @foreach($output_payer as $key => $value)
  <tr>
  <td>{{$key}}</td> 
    @foreach($value as $dohod_key => $dohod)
      @if($dohod_key == 'dohod')
      <td>@contert_currency($dohod)</td>
      @endif
    @endforeach
  @endforeach
  </tr>
</table>
  ">
    @contert_currency($dohod_sum) ₽ (@contert_currency($dohods))
  </div>
  <div class="flex-fill bg-danger text-white py-1 px-2 border-right border-white text-center" data-container="body" data-toggle="popover" data-placement="bottom" data-content="
  <table class='table table-bordered table-sm'>
    <table class='table table-bordered table-sm'>
      <thead class='table-danger'>
      <tr>
        <th scope='col' colspan='2'>Расходы</th>
      </tr>
    </thead>
  @foreach($output_payer as $key => $value)
  <tr>
  <td>{{$key}}</td> 
    @foreach($value as $rashod_key => $rashod)
      @if($rashod_key == 'rashod')
      <td>@contert_currency($rashod)</td>
      @endif
    @endforeach
  @endforeach
  </tr>
</table>
  ">
    @contert_currency($rashod_sum) ₽ (@contert_currency($rashods), <i class="fas fa-sad-cry"></i> - @contert_currency($compulsory_rashod_sum) ₽)
  </div>
  <div class="flex-fill{{ $income <= 0 ? ' bg-danger' : ' bg-success'}} text-white py-1 px-2 border-right border-white text-center" data-container="body" data-toggle="popover" data-placement="bottom" data-content="
  <table class='table table-bordered table-sm'>
    <table class='table table-bordered table-sm'>
      <thead class='table-info'>
      <tr>
        <th scope='col' colspan='2'>Прибыли</th>
      </tr>
    </thead>
  @foreach($output_payer as $key => $value)
  <tr>
  <td>{{$key}}</td> 
    @foreach($value as $rashod_key => $rashod)
      @if($rashod_key == 'profit')
      <td>@contert_currency($rashod)</td>
      @endif
    @endforeach
  @endforeach
  </tr>
</table>
  ">
    <b>
      <i class="fas fa-calculator"></i> @contert_currency($income) ₽
      </b>
  </div>
  <div class="flex-fill {{ $invest_class }} text-white py-1 px-2 border-right border-white text-center" data-container="body" data-toggle="popover" data-placement="bottom" data-content="
  <table class='table table-bordered table-sm'>
    <table class='table table-bordered table-sm'>
      <thead class='table-info'>
      <tr>
        <th scope='col' colspan='2'>Инвестировано</th>
      </tr>
    </thead>
  @foreach($output_payer as $key => $value)
  <tr>
  <td>{{$key}}</td> 
    @foreach($value as $rashod_key => $rashod)
      @if($rashod_key == 'invested')
      <td>@contert_currency($rashod)</td>
      @endif
    @endforeach
  @endforeach
  </tr>
</table>
  ">
      <i class="fas fa-coins"></i> @contert_currency($invested) ₽ ( <i class="fas fa-thumbs-up"></i> @contert_currency($underinvested) ₽)
  </div>
  <div class="flex-fill text-success py-1 px-2 bg-warning text-center rounded-right" data-container="body" data-toggle="popover" data-placement="bottom" data-content="
  <table class='table table-bordered table-sm'>
    <thead>
      <tr>
        <th>
          Счёт
        </th>
        <th>
          Баланс
        </th>
      </tr>
    </thead>
    <tbody>
      @foreach($accounts as $account)
        <tr>
          <td>
              <a href='{{ action('Member\AccountsController@edit', $account->id) }}'>{{$account->name}}</a>
          </td>
          <td>
              @contert_currency($account->balance)
          </td>
        </tr>
      @endforeach
      <tr>
        <td>
          Всего
        </td>
        <th>
            @contert_currency($total_balance)
        </th>
      </tr>
    </tbody>
  </table>
  ">
    <b><i class="fas fa-wallet"></i> @contert_currency($total_balance)</b>
  </div>
</div>
@endsection
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12 mt-2">
      @foreach ($errors->all() as $error)
          <p class="alert alert-danger">{{ $error }}</p>
      @endforeach
    </div>
  </div>
</div>
<div class="container mt-2">
  <div class="card">
    <div class="card-header ">
      <h5 class="float-left m-0">{{ trans('transactions.title') }} <button type="button" class="btn btn-link p-0 m-0" data-toggle="modal" data-target="#filterModal"><i class="fas fa-filter"></i></button></h5>
    </div>
    <div class="card-body mt-2">
      @if (session('status'))
      <div class="alert alert-success">
          {{ session('status') }}
      </div>
      @endif
        @if ($transactions->isEmpty())
          <p>{{ trans('transactions.nodata') }}</p>
        @else
          <table class="table table-bordered table-sm table-responsive-sm">
            @foreach($transactions as $transaction)
              <tr>
                <td class="created_date">{{ \Carbon\Carbon::parse($transaction->created_date)->format('d.m.Y')}}
                </td>
                <td nowrap class="type @if ($transaction->type->id == 1) bg-danger @elseif ($transaction->type->id == 2) bg-success @elseif ($transaction->type->id == 3) bg-warning @endif">
                  <a class="@if ($transaction->type->id == 1 or $transaction->type->id == 2 )text-white @elseif ($transaction->type->id == 3) text-dark @endif" href="{{ action('Member\ReportsController@transactionsByFilter', ['type' => $transaction->type->id]) }}">@contert_currency($transaction->sum)</a>
                </td>
                <td nowrap class="state">
                  <a class="text-dark" href="{{ action('Member\ReportsController@transactionsByFilter', ['state' => $transaction->state->id]) }}">{{ $transaction->state->name }}</a>
                  @if ($transaction->type->id == 1)
                    @if ($transaction->compulsory !== 1)
                    <i class="fas fa-surprise text-danger"></i>
                    @endif
                  @endif
                </td>
                <td class="description">
                  {{ $transaction->description }}
                </td>
                <td class="payer">
                  <a class="text-dark" href="{{ action('Member\ReportsController@transactionsByFilter', ['payer' => $transaction->payer->id]) }}">{{ $transaction->payer->name }}</a>
                </td>
                <td class="account">
                  <a class="text-dark" href="{{ action('Member\ReportsController@transactionsByFilter', ['account' => $transaction->account->id]) }}">{{ $transaction->account->name }}</a>
                  @if($transaction->account_to_id != NULL)
                    -> {{$accounts->find($transaction->account_to_id)->name}}
                  @endif
                </td>
                <td nowrap>
                  <a href="{{ action('Member\TransactionsController@edit', $transaction->id) }}"><i class="fas fa-cog"></i></a>
                  <span href="#" data-container="body" data-toggle="popover" data-placement="top" data-content='                      
                  Удалить?                      
                  {!! Form::open(["class" => "form d-inline", "method" => "post", "action" => ["Member\TransactionsController@destroy", $transaction->id ]]) !!}
                  {!! Form::submit("Да", [
                    "class" => "btn p-0 d-inline btn-link text-danger",
                    ])
                  !!}
                  '><i class="fas fa-trash-alt text-danger"></i></span>
                </td>
              </tr>
            @endforeach
          </table>
          {{ $transactions->links() }}
        @endif
    </div>
  </div>
</div>

{{-- Modal for Transactions --}}
<div class="modal fade" id="incomeModal" tabindex="-1" role="dialog" aria-labelledby="incomeModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="incomeModal">{{trans('transactions.add_income')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      {!! Form::open(['class' => 'form', 'method' => 'post', 'action' => 'Member\TransactionsController@store']) !!}
      <div class="modal-body">
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::text('created_date', \Carbon\Carbon::now()->format('d.m.Y H:i:s'), [
          'class' => 'form-control',
          'id' => 'datepicker',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::text('sum', null, [
          'placeholder' => trans('transactions.sum'),
          'class' => 'form-control',
          'id' => 'sum',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::hidden('type_id', 2, [
          'class' => 'form-control',
          'id' => 'type_id',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::select('state_id', App\State::orderBy('name','asc')->pluck('name','id'), null, [
          'class' => 'form-control',
          'id' => 'state_id',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::text('description', null, [
          'class' => 'form-control',
          'id' => 'description',
          'placeholder' => trans('transactions.description')
          ])
        !!}
        </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::select('account_id', App\Account::orderBy('name','asc')->pluck('name','id'), null, [
          'class' => 'form-control',
          'id' => 'account_id',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::select('payer_id', App\Payer::orderBy('name','asc')->pluck('name','id'), null, [
          'class' => 'form-control',
          'id' => 'payer_id',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{trans('main.close')}}</button>
        {!! Form::submit(trans('transactions.add'), [
          'class' => 'btn btn-primary'
          ])
        !!}
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
{{-- Modal for Expense --}}
<div class="modal fade" id="expenseModal" tabindex="-1" role="dialog" aria-labelledby="expenseModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="expenseModal">{{trans('transactions.add_expense')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      {!! Form::open(['class' => 'form', 'method' => 'post', 'action' => 'Member\TransactionsController@store']) !!}
      <div class="modal-body">
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::text('created_date', \Carbon\Carbon::now()->format('d.m.Y H:i:s'), [
          'class' => 'form-control',
          'id' => 'datepicker',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::text('sum', null, [
          'placeholder' => trans('transactions.sum'),
          'class' => 'form-control',
          'id' => 'sum',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::hidden('type_id', 1, [
          'class' => 'form-control',
          'id' => 'type_id',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::select('state_id', App\State::orderBy('name','asc')->pluck('name','id'), null, [
          'class' => 'form-control',
          'id' => 'state_id',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::text('description', null, [
          'class' => 'form-control',
          'id' => 'description',
          'placeholder' => trans('transactions.description')
          ])
        !!}
        </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::select('account_id', App\Account::orderBy('name','asc')->pluck('name','id'), null, [
          'class' => 'form-control',
          'id' => 'account_id',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::select('payer_id', App\Payer::orderBy('name','asc')->pluck('name','id'), null, [
          'class' => 'form-control',
          'id' => 'payer_id',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
            <span class="switch">
              {!! Form::checkbox('compulsory', '1', false, [
                'id' => 'compulsoryCheck',
                'class' => 'switch'
                ]); !!}
              <label class="switch-normal" for="compulsoryCheck">
                Обязательно?
              </label>
            </span>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{trans('main.close')}}</button>
        {!! Form::submit(trans('transactions.add'), [
          'class' => 'btn btn-primary'
          ])
        !!}
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
{{-- Modal for Transfer --}}
<div class="modal fade" id="transferModal" tabindex="-1" role="dialog" aria-labelledby="transferModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="transferModal">{{trans('transactions.add_transfer')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      {!! Form::open(['class' => 'form', 'method' => 'post', 'action' => 'Member\TransactionsController@store']) !!}
      <div class="modal-body">
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::text('created_date', \Carbon\Carbon::now()->format('d.m.Y H:i:s'), [
          'class' => 'form-control',
          'id' => 'datepicker',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::text('sum', null, [
          'placeholder' => trans('transactions.sum'),
          'class' => 'form-control',
          'id' => 'sum',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::hidden('type_id', 3, [
          'class' => 'form-control',
          'id' => 'type_id',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::select('state_id', App\State::orderBy('name','asc')->pluck('name','id'), null, [
          'class' => 'form-control',
          'id' => 'state_id',
          'placeholder' => 'Статья',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::text('description', null, [
          'class' => 'form-control',
          'id' => 'description',
          'placeholder' => trans('transactions.description')
          ])
        !!}
        </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::select('account_id', App\Account::orderBy('name','asc')->pluck('name','id'), null, [
          'class' => 'form-control',
          'id' => 'account_id',
          'placeholder' => 'Со счёта',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::select('account_to_id', App\Account::orderBy('name','asc')->pluck('name','id'), null, [
          'class' => 'form-control',
          'id' => 'account_to_id',
          'placeholder' => 'На счёт',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::select('payer_id', App\Payer::orderBy('name','asc')->pluck('name','id'), null, [
          'class' => 'form-control',
          'id' => 'payer_id',
          'placeholder' => 'Плательщик',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
            <span class="switch">
              {!! Form::checkbox('compulsory', '1', false, [
                'id' => 'compulsoryCheck2',
                'class' => 'switch'
                ]); !!}
              <label class="switch-normal" for="compulsoryCheck2">
                Обязательно?
              </label>
            </span>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{trans('main.close')}}</button>
        {!! Form::submit(trans('transactions.add'), [
          'class' => 'btn btn-primary'
          ])
        !!}
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
{{-- Modal for Filter --}}
<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterrModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5>Фильтровать</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      {!! Form::open(['class' => 'form', 'method' => 'get', 'action' => 'Member\ReportsController@transactionsByFilter']) !!}
      <div class="modal-body">
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::text('date', false, [
          'class' => 'form-control',
          'id' => 'datepicker',
          'placeholder' => \Carbon\Carbon::now()->format('Y-m-d')
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::select('type', App\Type::orderBy('name','asc')->pluck('name','id'), null, [
          'class' => 'form-control',
          'id' => 'type',
          'placeholder' => 'Тип'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::select('state', App\State::orderBy('name','asc')->pluck('name','id'), null, [
          'class' => 'form-control',
          'id' => 'state',
          'placeholder' => 'Статья'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::select('account', App\Account::orderBy('name','asc')->pluck('name','id'), null, [
          'class' => 'form-control',
          'id' => 'account',
          'placeholder' => 'Счёт'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::select('payer', App\Payer::orderBy('name','asc')->pluck('name','id'), null, [
          'class' => 'form-control',
          'id' => 'payer',
          'placeholder' => 'Плательщик'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
            <div class="col-lg-12">
        {!! Form::submit('Фильтровать', [
          'class' => 'btn btn-primary'
          ])
        !!}
        </div>
      </div>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection
