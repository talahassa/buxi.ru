@extends('layouts.app')
@section('title', trans('types.title'))
@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-8 mt-2">
      <div class="card">
        <div class="card-header ">
          <h5 class="float-left m-0">{{ trans('types.title') }}</h5>
        </div>
        <div class="card-body mt-2">
          @if (session('status'))
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
          @endif
            @if ($types->isEmpty())
              <p>{{ trans('types.nodata') }}</p>
            @else
              <ul>
                @foreach($types as $type)
                  <li><a href="{{ action('Member\TypesController@edit', $type->id) }}">{{ $type->name }}</a></li>
                @endforeach
              </ul>
            @endif
        </div>
      </div>
    </div>
    <div class="col-md-4 mt-2">
        <div class="card">
            <div class="card-header ">
                <h5 class="float-left m-0">{{ trans('types.add') }}</h5>
            </div>
            <div class="card-body mt-2">
              @foreach ($errors->all() as $error)
                  <p class="alert alert-danger">{{ $error }}</p>
              @endforeach
              {!! Form::open(['class' => 'form', 'method' => 'post', 'action' => 'Member\TypesController@store']) !!}
              <div class="form-group">
                <div class="col-lg-12">
              {!! Form::text('name', null, [
                'class' => 'form-control',
                'id' => 'name',
                'placeholder' => trans('types.title_name')
                ])
              !!}
              </div>
              </div>
              <div class="col-lg-12">
              <div class="form-group">
              {!! Form::submit(trans('types.add'), [
                'class' => 'btn btn-primary'
                ])
              !!}
              </div>
              </div>

              {!! Form::close() !!}
            </div>
        </div>
    </div>
  </div>
</div>
@endsection
