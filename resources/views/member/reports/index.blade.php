@extends('layouts.app')
@section('title', trans('reports.title'))
@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-8 mt-2">
      <div class="card">
        <div class="card-header ">
          <h5 class="float-left m-0">{{ trans('reports.title') }}</h5>
        </div>
        <div class="card-body mt-2">
          @if (session('status'))
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
          @endif
          <h6>Стандартные</h6>
          <ul>
            <li>
              <a href="{{ action('Member\ReportsController@thisMonth')}}">Текущий месяц</a>
            </li>
            <li>
              <a href="{{ action('Member\ReportsController@previousMonth')}}">Предыдущий месяц</a>
            </li>
            <li>
              <a href="{{ action('Member\ReportsController@thisYear')}}">Текущий год</a>
            </li>
          </ul>
            @if ($reports->isEmpty())
              <p>{{ trans('reports.nodata') }}</p>
            @else
              <h6>Пользовательские</h6>
              <ul>
                @foreach($reports as $report)
                  <li class="mb-2"><a href="{{ action('Member\ReportsController@show', $report->id) }}">{{ \Carbon\Carbon::parse($report->start_date)->format('d.m.Y')}} - {{ \Carbon\Carbon::parse($report->end_date)->format('d.m.Y')}} {{$report->title}}</a> {!! Form::open(['class' => 'form float-right', 'method' => 'post', 'action' => ['Member\ReportsController@destroy', $report->id ]]) !!}
                  <button class="btn btn-link p-0"><small><i class="fas fa-trash-alt"></i></small></button>{!! Form::close() !!}</li>
                @endforeach
              </ul>
            @endif
        </div>
      </div>
    </div>
    <div class="col-md-4 mt-2">
      <div class="card">
        <div class="card-header ">
            <h5 class="float-left m-0">{{ trans('reports.create') }}</h5>
        </div>
        <div class="card-body mt-2">
        @foreach ($errors->all() as $error)
            <p class="alert alert-danger">{{ $error }}</p>
        @endforeach
        {!! Form::open(['class' => 'form', 'method' => 'post', 'action' => 'Member\ReportsController@store']) !!}
        <div class="col-lg-12">
          <div class="form-group">
        {!! Form::text('start_date', \Carbon\Carbon::now()->format('d.m.Y'), [
          'class' => 'form-control',
          'id' => 'start_date'
          ])
        !!}
          </div>
        </div>
        <div class="col-lg-12">
          <div class="form-group">
        {!! Form::text('end_date', \Carbon\Carbon::now()->format('d.m.Y'), [
          'class' => 'form-control',
          'id' => 'end_date'
          ])
        !!}
          </div>
        </div>
        <div class="col-lg-12">
          <div class="form-group">
        {!! Form::text('title', '', [
          'class' => 'form-control',
          'id' => 'title',
          'placeholder' => 'Заголовок по желанию'
          ])
        !!}
          </div>
        </div>
        <div class="col-lg-12">
          <div class="form-group">
        {!! Form::submit(trans('main.add'), [
          'class' => 'btn btn-primary'
          ])
        !!}
          </div>
        </div>
        {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection
