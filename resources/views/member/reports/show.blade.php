@extends('layouts.app')
@section('title', trans('reports.title'))
@section('content')

<div class="container-fluid">
  <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ action('Member\TransactionsController@index') }}">{{trans('transactions.title')}}</a></li>
    <li class="breadcrumb-item"><a href="{{ action('Member\ReportsController@index') }}">{{trans('reports.title')}}</a></li>
  </ol>
</nav>
  <div class="row">
    <div class="col-md-6 mt-2">
      <div class="card">
        <div class="card-header ">
          {{ trans('reports.single') }}: {{ \Carbon\Carbon::parse($report->start_date)->format('d.m.Y')}} - {{ \Carbon\Carbon::parse($report->end_date)->format('d.m.Y')}}
        </div>
        <div class="card-body mt-2">
          @if (session('status'))
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
          @endif
            @if ($transactions->isEmpty())
              <p>{{ trans('reports.nodata') }}</p>
            @else
              <table class="table table-bordered table-sm table-responsive-sm">
                @foreach($transactions as $transaction)
                  <tr>
                    <td class="created_date"><a href="{{ action('Member\TransactionsController@edit', $transaction->id) }}">{{ \Carbon\Carbon::parse($transaction->created_date)->format('d.m.Y')}}</a>
                    </td>
                    <td class="sum text-nowrap">
                        @contert_currency($transaction->sum)
                    </td>
                    <td class="type @if ($transaction->type->id == 1) bg-danger text-white @elseif ($transaction->type->id == 2) bg-success  text-white @elseif ($transaction->type->id == 3) bg-warning @endif">
                      {{ $transaction->type->name }}
                    </td>
                    <td class="state">
                      {{ $transaction->state->name }}
                    </td>
                    <td class="description">
                      {{ $transaction->description }}
                    </td>
                    <td class="payer">
                      {{ $transaction->payer->name }}
                    </td>
                    <td class="account">
                      {{ $transaction->account->name }}
                      @if($transaction->account_to_id != NULL)
                        -> {{$accounts->find($transaction->account_to_id)->name}}
                      @endif
                    </td>
                    <td class="compulsory">
                      @if ($transaction->compulsory == 1)
                        {{trans('main.yes')}}
                      @endif
                    </td>
                  </tr>
                @endforeach
              </table>
            @endif
        </div>
      </div>
    </div>
    <div class="col-md-2 mt-2">
      <div class="card">
        <div class="card-header ">
          Доходы
        </div>
        <div class="card-body mt-2">
          <table class="table table-bordered table-sm">
            <tbody>
            @php($counter_dohod_total = 0)
            @foreach ($states as $state)
              @php($counter_dohod = 0)
              @foreach ($state->transactions as $transaction)
                  @if ($transaction->type_id == 2)
                    @php($counter_dohod += $transaction->sum)
                    @php($counter_dohod_total += $transaction->sum)
                  @endif
              @endforeach
              @if($counter_dohod > 0)
                <tr>
                  <td>
                    {{$state->name}}
                  </td>
                  <td>
                    @contert_currency($counter_dohod) ₽
                  </td>
                </tr>
                @endif
            @endforeach
            <tr>
              <th>
                  {{ trans('main.total') }}
              </th>
              <th>
                @contert_currency($counter_dohod_total) ₽
              </th>
            </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-md-2 mt-2">
      <div class="card">
        <div class="card-header ">
          Расходы
        </div>
        <div class="card-body mt-2">
          @if (session('status'))
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
          @endif
            @if ($transactions->isEmpty())
              <p>{{ trans('transactions.nodata') }}</p>
            @else
              <table class="table table-bordered table-sm">
                <tbody>
                @php($counter_rashod_total = 0)
                @foreach ($states as $state)
                  @php($counter_rashod = 0)
                  @foreach ($state->transactions as $transaction)
                      @if ($transaction->type_id == 1)
                        @php($counter_rashod += $transaction->sum)
                        @php($counter_rashod_total += $transaction->sum)
                      @endif
                  @endforeach
                  @if($counter_rashod > 0)
                    <tr>
                      <td>
                        {{$state->name}}
                      </td>
                      <td>
                        @contert_currency($counter_rashod) ₽
                      </td>
                    </tr>
                    @endif
                @endforeach
                <tr>
                  <th>
                      {{ trans('main.total') }}
                  </th>
                  <th>
                    @contert_currency($counter_rashod_total) ₽
                  </th>
                </tr>
            </tbody>
          </table>
          @endif
        </div>
      </div>
    </div>
    <div class="col-md-2 mt-2">
      <div class="card">
        <div class="card-header ">
          Необязательные расходы
        </div>
        <div class="card-body mt-2">
          <table class="table table-bordered table-sm">
            <tbody>
              @php($compulsories = 0)
            @foreach ($states as $state)
              @php($counter_rashod = 0)
              @foreach ($state->transactions as $transaction)
                  @if ($transaction->type_id == 1)
                  @if ($transaction->compulsory != 1)
                    @php($counter_rashod += $transaction->sum)
                    @php($compulsories += 1)
                  @endif
                  @endif
              @endforeach
              @if($counter_rashod > 0)
                <tr>
                  <td>
                    {{$state->name}}
                  </td>
                  <td>
                    @contert_currency($counter_rashod) ₽
                  </td>
                </tr>
                @endif
            @endforeach
            @if($compulsories == 0)
              <div>За текущий период не было зафиксировано необязательных расходов!
              </div>
              <div class="success">Поздравляем! Вы истинный адепт культа "Не отдать!"</div>
            @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
</div>
@endsection
