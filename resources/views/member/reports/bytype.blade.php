@extends('layouts.app')
@section('title', trans('transactions.title'))
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12 mt-2">
      @foreach ($errors->all() as $error)
          <p class="alert alert-danger">{{ $error }}</p>
      @endforeach
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
        {!! Form::open(['class' => 'form-inline', 'method' => 'get', 'action' => 'Member\ReportsController@transactionsByFilter']) !!}
          <div class="form-group">
            <div class="col-lg-12">
          {!! Form::text('date', false, [
            'class' => 'form-control',
            'id' => 'datepicker',
            'placeholder' => \Carbon\Carbon::now()->format('Y-m-d')
            ])
          !!}
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-12">
          {!! Form::select('type', App\Type::orderBy('name','asc')->pluck('name','id'), null, [
            'class' => 'form-control',
            'id' => 'type',
            'placeholder' => 'Тип'
            ])
          !!}
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-12">
          {!! Form::select('state', App\State::orderBy('name','asc')->pluck('name','id'), null, [
            'class' => 'form-control',
            'id' => 'state',
            'placeholder' => 'Статья'
            ])
          !!}
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-12">
          {!! Form::select('account', App\Account::orderBy('name','asc')->pluck('name','id'), null, [
            'class' => 'form-control',
            'id' => 'account',
            'placeholder' => 'Счёт'
            ])
          !!}
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-12">
          {!! Form::select('payer', App\Payer::orderBy('name','asc')->pluck('name','id'), null, [
            'class' => 'form-control',
            'id' => 'payer',
            'placeholder' => 'Плательщик'
            ])
          !!}
            </div>
          </div>
          <div class="form-group">
              <div class="col-lg-12">
          {!! Form::submit('Фильтровать', [
            'class' => 'btn btn-primary'
            ])
          !!}
          </div>
        </div>
        {!! Form::close() !!}
    </div>
  </div>
  <div class="row">
    <div class="col-md-8 mt-2">
      <div class="card">
        <div class="card-header ">
          <h5 class="float-left m-0">{{ trans('transactions.title') }}</h5>
        </div>
        <div class="card-body mt-2">
          @if (session('status'))
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
          @endif
            @if ($transactions->isEmpty())
              <p>{{ trans('transactions.nodata') }}</p>
            @else
              <table class="table table-bordered table-sm table-responsive-sm">
                @foreach($transactions as $transaction)
                  <tr>
                    <td class="created_date">{{ \Carbon\Carbon::parse($transaction->created_date)->format('d.m.Y')}}
                    </td>
                    <td class="sum text-nowrap">
                        @contert_currency($transaction->sum)
                    </td>
                    <td class="type @if ($transaction->type->id == 1) bg-danger text-white @elseif ($transaction->type->id == 2) bg-success  text-white @elseif ($transaction->type->id == 3) bg-warning @endif">
                      {{ $transaction->type->name }}
                    </td>
                    <td class="state">
                      {{ $transaction->state->name }}
                    </td>
                    <td class="description">
                      {{ $transaction->description }}
                    </td>
                    <td class="payer">
                      {{ $transaction->payer->name }}
                    </td>
                    <td class="account">
                      {{ $transaction->account->name }}
                      @if($transaction->account_to_id != NULL)
                        -> {{$accounts->find($transaction->account_to_id)->name}}
                      @endif
                    </td>
                    <td class="compulsory">
                      @if ($transaction->type->id == 1)
                        @if ($transaction->compulsory == 1)
                        <i class="fas fa-check text-info"></i>
                        @elseif ($transaction->compulsory !== 1)
                        <i class="fas fa-exclamation text-danger"></i>
                        @endif
                      @elseif ($transaction->type->id == 2)
                      <i class="far fa-thumbs-up text-success"></i>
                      @elseif ($transaction->type->id == 3)
                      <i class="fas fa-exchange-alt text-info"></i>
                      @endif
                    </td>
                    <td><a href="{{ action('Member\TransactionsController@edit', $transaction->id) }}"><i class="fas fa-cog"></i></td>
                  </tr>
                @endforeach
              </table>
              {{ $transactions->links() }}
            @endif
        </div>
      </div>
    </div>
    <div class="col-md-4 col-sm-12 col-xs-12 mt-2">
        @php($total_sum = 0)
        @foreach($transactions as $transaction)
        @php($total_sum += $transaction->sum)
        @endforeach
        <table class="table table-bordered table-sm">
            <tbody>
                <tr>
                  <th>
                    Всего
                </th>
                  <td>
                      @contert_currency($total_sum) ₽
                  </td>
                </tr>
        </table>
      
    </div>
  </div>
</div>

@endsection
