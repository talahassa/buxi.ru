@extends('layouts.app')
@section('title', trans('transactions.title'))

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12 mt-2">
      @foreach ($errors->all() as $error)
          <p class="alert alert-danger">{{ $error }}</p>
      @endforeach
    </div>
  </div>
</div>
<div class="container-fluid">
  <h1 class="h3">Статистика за предыдущий месяц</h1>
  <div class="row">
    <div class="col-md-2 mt-2">
      <div class="card">
        <div class="card-header ">
          <h5 class="float-left m-0">Доходы</h5>
        </div>
        <div class="card-body mt-2">
          <table class="table table-bordered table-sm">
            <tbody>
            @php($counter_dohod_total = 0)
            @foreach ($states as $state)
              @php($counter_dohod = 0)
              @foreach ($state->transactions as $transaction)
                  @if ($transaction->type_id == 2)
                    @php($counter_dohod += $transaction->sum)
                    @php($counter_dohod_total += $transaction->sum)
                  @endif
              @endforeach
              @if($counter_dohod > 0)
                <tr>
                  <td>
                    {{$state->name}}
                  </td>
                  <td>
                    @contert_currency($counter_dohod) ₽
                  </td>
                </tr>
                @endif
            @endforeach
            <tr>
                <th>
                  {{ trans('main.total') }}
                </th>
                <th>
                  @contert_currency($counter_dohod_total) ₽
                </th>
              </tr>
            </tbody>
          </table>
          <div style="width: 100%">
              {!! $dohod_chart->container() !!} </div>
        </div>
      </div>
    </div>
    <div class="col-md-2 mt-2">
      <div class="card">
        <div class="card-header ">
          <h5 class="float-left m-0">Расходы</h5>
        </div>
        <div class="card-body mt-2">
          @if (session('status'))
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
          @endif
            @if ($transactions->isEmpty())
              <p>{{ trans('transactions.nodata') }}</p>
            @else
              <table class="table table-bordered table-sm">
                <tbody>
                @foreach ($states as $state)
                  @php($counter_rashod = 0)
                  @foreach ($state->transactions as $transaction)
                      @if ($transaction->type_id == 1)
                        @php($counter_rashod += $transaction->sum)
                      @endif
                  @endforeach
                  @if($counter_rashod > 0)
                    <tr>
                      <td>
                        {{$state->name}}
                      </td>
                      <td>
                        @contert_currency($counter_rashod) ₽
                      </td>
                    </tr>
                    @endif
                @endforeach
            </tbody>
          </table>
          <div style="width: 100%">
            {!! $rashod_chart->container() !!} 
          </div>
          @endif
        </div>
      </div>
    </div>
    <div class="col-md-2 mt-2">
      <div class="card">
        <div class="card-header ">
          <h5 class="float-left m-0">Необязательные расходы</h5>
        </div>
        <div class="card-body mt-2">
          <table class="table table-bordered table-sm">
            <tbody>
              @php($compulsories = 0)
            @foreach ($states as $state)
              @php($counter_rashod = 0)
              @foreach ($state->transactions as $transaction)
                  @if ($transaction->type_id == 1)
                  @if ($transaction->compulsory != 1)
                    @php($counter_rashod += $transaction->sum)
                    @php($compulsories += 1)
                  @endif
                  @endif
              @endforeach
              @if($counter_rashod > 0)
                <tr>
                  <td>
                    {{$state->name}}
                  </td>
                  <td>
                    @contert_currency($counter_rashod) ₽
                  </td>
                </tr>
                @endif
            @endforeach
            @if($compulsories == 0)
              <div>За текущий период не было зафиксировано необязательных расходов!
              </div>
              <div class="success">Поздравляем! Вы истинный адепт культа "Не отдать!"</div>
            @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-md-4 col-sm-12 col-xs-12 mt-2">
      <div class="card">
        <div class="card-header ">
          <h5 class="float-left m-0">{{ trans('main.info') }}</h5>
        </div>
        <div class="card-body mt-2">
          @php($dohods = 0)
          @php($rashods = 0)
          @php($rashod_sum = 0)
          @php($dohod_sum = 0)
          @php($income = 0)
          @php($invested = 0)
          @php($didek_sum = 0)
          @php($nargiza_sum = 0)
          @php($all_sum = 0)
          @php($didek_min = 0)
          @php($nargiza_min = 0)
          @php($all_min = 0)
          @php($compulsory_rashods = 0)
          @php($compulsory_rashod_sum = 0)
          @foreach($transactions as $transaction)
            {{-- Rashod --}}
            @if ($transaction->type->id == 1)
              @php($rashods += 1)
              @php($rashod_sum += $transaction->sum)
              @if($transaction->compulsory != 1)
                @php($compulsory_rashods += 1)
                @php($compulsory_rashod_sum += $transaction->sum)
              @endif
            @endif
            @if ($transaction->type->id == 2)
              @php($dohods += 1)
              @php($dohod_sum += $transaction->sum)
            @endif
            @if ($transaction->type->id == 3)
              @if ($transaction->state->id == 2)
                @php($invested += $transaction->sum)
              @endif
            @endif
          @endforeach
          @php($income = $dohod_sum - $rashod_sum)
          <div class="row">
            <div class="col-md-6">
              <table class="table table-bordered table-sm">
                <tbody>
                  <tr>
                    <th>
                    {{ trans('main.income') }}
                  </th>
                    <td>
                    {{ $dohods }}
                    </td>
                  </tr>
                  <tr>
                    <th>
                    На сумму
                  </th>
                    <td>
                        @contert_currency($dohod_sum) ₽
                    </td>
                  </tr>
                  <tr>
                    <th>
                    Прибыль
                  </th>
                    <td>
                      @contert_currency($dohod_sum - $rashod_sum) ₽
                    </td>
                  </tr>
                  <tr>
                    <th>
                    Инвестировано
                  </th>
                    <td>
                      @contert_currency($invested) ₽
                    </td>
                  </tr>
                </tbody>
              </table>
              <table class="table table-bordered table-sm">
                <tbody>
                  <tr>
                    <th>
                    {{ trans('main.expence') }}
                  </th>
                    <td>
                        @contert_currency($rashods)
                    </td>
                  </tr>
                  <tr>
                    <th>
                    На сумму
                  </th>
                    <td>
                        @contert_currency($rashod_sum) ₽
                    </td>
                  </tr>
                  <tr>
                    <th>
                    Необязательных
                  </th>
                    <td>
                        @contert_currency($compulsory_rashod_sum) ₽
                    </td>
                  </tr>
                </tbody>
              </table>
              @foreach ($payers as $payer)
                @if($payer->id == 3)
                    @foreach ($types as $type)
                      @php($both_sum = 0)
                      @php($both_minus = 0)
                      @php($both_invest = 0)
                      @foreach ($payer->transactions as $transaction)
                      @if ($transaction->type_id == 1)
                        @php($both_minus += $transaction->sum)
                      @endif
                      @if ($transaction->type_id == 2)
                        @php($both_sum += $transaction->sum)
                      @endif
                      @if ($transaction->type_id == 3)
                        @php($both_invest += $transaction->sum)
                      @endif
                    @endforeach
                    @endforeach
                @endif
                @endforeach
                @foreach ($payers as $payer)
                @if($payer->id != 3)
                <h6 class="float-left m-0">{{$payer->name}}</h6>
                <table class="table table-bordered table-sm">
                  <tbody>
                    @foreach ($types as $type)
                      @php($counter_rashod = 0)
                      @php($counter_dohod = 0)
                      @php($counter_invest = 0)
                      @foreach ($payer->transactions as $transaction)
                          {{-- @if ($transaction->type_id == $type->id)
                            @php($counter[$type->id] += $transaction->sum)
                          @endif --}}
                          @if ($transaction->type_id == 1)
                            @php($counter_rashod += $transaction->sum)
                           @endif
                          @if ($transaction->type_id == 2)
                            @php($counter_dohod += $transaction->sum)
                           @endif
                          @if ($transaction->type_id == 3 && $transaction->state_id == 2)
                            @php($counter_invest += $transaction->sum)
                           @endif
                      @endforeach
                    @endforeach
                    <tr>
                      <th>
                        Доход
                      </th>
                      <td>
                        @contert_currency($counter_dohod + $both_sum / 2) ₽
                      </td>
                    </tr>
                    <tr>
                      <th>
                        Расход
                      </th>
                      <td>
                        @contert_currency($counter_rashod + $both_minus / 2) ₽
                      </td>
                    </tr>
                    <tr>
                      <th>
                        Прибыль
                      </th>
                      <td>
                        @contert_currency(($counter_dohod + $both_sum / 2) - ($counter_rashod + $both_minus / 2)) ₽
                      </td>
                    </tr>
                    <tr>
                      <th>
                        Инвестировано
                      </th>
                      <td>
                        @contert_currency($counter_invest + $both_invest / 2) ₽
                      </td>
                    </tr>
                  </tbody>
                </table>
              @else
                <h6 class="float-left m-0">{{$payer->name}}</h6>
                <table class="table table-bordered table-sm">
                  <tbody>
                    @foreach ($types as $type)
                      @php($counter_rashod = 0)
                      @php($counter_dohod = 0)
                      @php($counter_invest = 0)
                      @foreach ($payer->transactions as $transaction)
                          {{-- @if ($transaction->type_id == $type->id)
                            @php($counter[$type->id] += $transaction->sum)
                          @endif --}}
                          @if ($transaction->type_id == 1)
                            @php($counter_rashod += $transaction->sum)
                           @endif
                          @if ($transaction->type_id == 2)
                            @php($counter_dohod += $transaction->sum)
                           @endif
                          @if ($transaction->type_id == 3 && $transaction->state_id == 2)
                            @php($counter_invest += $transaction->sum)
                           @endif
                      @endforeach
                    @endforeach
                    <tr>
                      <th>
                        Доход
                      </th>
                      <td>
                        @contert_currency($counter_dohod) ₽
                      </td>
                    </tr>
                    <tr>
                      <th>
                        Расход
                      </th>
                      <td>
                        @contert_currency($counter_rashod) ₽
                      </td>
                    </tr>
                    <tr>
                      <th>
                        Прибыль
                      </th>
                      <td>
                        @contert_currency($counter_dohod + $counter_rashod) ₽
                      </td>
                    </tr>
                    <tr>
                      <th>
                        Инвестировано
                      </th>
                      <td>
                        @contert_currency($counter_invest) ₽
                      </td>
                    </tr>
                  </tbody>
                </table>
                @endif
              @endforeach
            </div>
            <div class="col-md-6">
              <table class="table table-bordered table-sm">
                <thead>
                  <tr>
                    <th>
                      Счёт
                    </th>
                    <th>
                      Баланс
                    </th>
                  </tr>
                </thead>
                <tbody>
                  @php($total_balance = 0)
                  @foreach($accounts as $account)
                    <tr>
                      <td>
                          <a href="{{ action('Member\AccountsController@edit', $account->id) }}">{{$account->name}}</a>
                      </td>
                      <td>
                          @contert_currency($account->balance)
                      </td>
                    </tr>
                    @php($total_balance += $account->balance)
                  @endforeach
                  <tr>
                    <td>
                      Всего
                    </td>
                    <th>
                        @contert_currency($total_balance)
                    </th>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

{{-- Modal for Transactions --}}
<div class="modal fade" id="incomeModal" tabindex="-1" role="dialog" aria-labelledby="incomeModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="incomeModal">{{trans('transactions.add_income')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      {!! Form::open(['class' => 'form', 'method' => 'post', 'action' => 'Member\TransactionsController@store']) !!}
      <div class="modal-body">
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::text('created_date', \Carbon\Carbon::now()->format('d.m.Y H:i:s'), [
          'class' => 'form-control',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::text('sum', null, [
          'placeholder' => trans('transactions.sum'),
          'class' => 'form-control',
          'id' => 'sum',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::hidden('type_id', 2, [
          'class' => 'form-control',
          'id' => 'type_id',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::select('state_id', App\State::orderBy('name','asc')->pluck('name','id'), null, [
          'class' => 'form-control',
          'id' => 'state_id',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::text('description', null, [
          'class' => 'form-control',
          'id' => 'description',
          'placeholder' => trans('transactions.description')
          ])
        !!}
        </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::select('account_id', App\Account::orderBy('name','asc')->pluck('name','id'), null, [
          'class' => 'form-control',
          'id' => 'account_id',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::select('payer_id', App\Payer::orderBy('name','asc')->pluck('name','id'), null, [
          'class' => 'form-control',
          'id' => 'payer_id',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{trans('main.close')}}</button>
        {!! Form::submit(trans('transactions.add'), [
          'class' => 'btn btn-primary'
          ])
        !!}
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
{{-- Modal for Expense --}}
<div class="modal fade" id="expenseModal" tabindex="-1" role="dialog" aria-labelledby="expenseModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="expenseModal">{{trans('transactions.add_expense')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      {!! Form::open(['class' => 'form', 'method' => 'post', 'action' => 'Member\TransactionsController@store']) !!}
      <div class="modal-body">
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::text('created_date', \Carbon\Carbon::now()->format('d.m.Y H:i:s'), [
          'class' => 'form-control',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::text('sum', null, [
          'placeholder' => trans('transactions.sum'),
          'class' => 'form-control',
          'id' => 'sum',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::hidden('type_id', 1, [
          'class' => 'form-control',
          'id' => 'type_id',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::select('state_id', App\State::orderBy('name','asc')->pluck('name','id'), null, [
          'class' => 'form-control',
          'id' => 'state_id',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::text('description', null, [
          'class' => 'form-control',
          'id' => 'description',
          'placeholder' => trans('transactions.description')
          ])
        !!}
        </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::select('account_id', App\Account::orderBy('name','asc')->pluck('name','id'), null, [
          'class' => 'form-control',
          'id' => 'account_id',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::select('payer_id', App\Payer::orderBy('name','asc')->pluck('name','id'), null, [
          'class' => 'form-control',
          'id' => 'payer_id',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
            <span class="switch">
              {!! Form::checkbox('compulsory', '1', false, [
                'id' => 'compulsoryCheck',
                'class' => 'switch'
                ]); !!}
              <label class="switch-normal" for="compulsoryCheck">
                Обязательно?
              </label>
            </span>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{trans('main.close')}}</button>
        {!! Form::submit(trans('transactions.add'), [
          'class' => 'btn btn-primary'
          ])
        !!}
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
{{-- Modal for Transfer --}}
<div class="modal fade" id="transferModal" tabindex="-1" role="dialog" aria-labelledby="transferModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="transferModal">{{trans('transactions.add_transfer')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      {!! Form::open(['class' => 'form', 'method' => 'post', 'action' => 'Member\TransactionsController@store']) !!}
      <div class="modal-body">
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::text('created_date', \Carbon\Carbon::now()->format('d.m.Y H:i:s'), [
          'class' => 'form-control',
          'id' => 'datepicker',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::text('sum', null, [
          'placeholder' => trans('transactions.sum'),
          'class' => 'form-control',
          'id' => 'sum',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::hidden('type_id', 3, [
          'class' => 'form-control',
          'id' => 'type_id',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::select('state_id', App\State::orderBy('name','asc')->pluck('name','id'), null, [
          'class' => 'form-control',
          'id' => 'state_id',
          'placeholder' => 'Статья',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::text('description', null, [
          'class' => 'form-control',
          'id' => 'description',
          'placeholder' => trans('transactions.description')
          ])
        !!}
        </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::select('account_id', App\Account::orderBy('name','asc')->pluck('name','id'), null, [
          'class' => 'form-control',
          'id' => 'account_id',
          'placeholder' => 'Со счёта',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::select('account_to_id', App\Account::orderBy('name','asc')->pluck('name','id'), null, [
          'class' => 'form-control',
          'id' => 'account_to_id',
          'placeholder' => 'На счёт',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
        {!! Form::select('payer_id', App\Payer::orderBy('name','asc')->pluck('name','id'), null, [
          'class' => 'form-control',
          'id' => 'payer_id',
          'placeholder' => 'Плательщик',
          'required' => 'required'
          ])
        !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-12">
            <span class="switch">
              {!! Form::checkbox('compulsory', '1', false, [
                'id' => 'compulsoryCheck2',
                'class' => 'switch'
                ]); !!}
              <label class="switch-normal" for="compulsoryCheck2">
                Обязательно?
              </label>
            </span>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{trans('main.close')}}</button>
        {!! Form::submit(trans('transactions.add'), [
          'class' => 'btn btn-primary'
          ])
        !!}
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

@endsection

@section('footer')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
{!! $dohod_chart->script() !!}
{!! $rashod_chart->script() !!}
@endsection