@extends('layouts.app')
@section('title', 'Редактировать плательщика')

@section('content')
    <div class="container col-md-8 col-md-offset-2">
        <div class="card mt-5">
            <div class="card-header ">
                <h5 class="float-left mb-0">{{ trans('payers.edit') }}</h5>
            </div>
            <div class="card-body mt-2">
              @foreach ($errors->all() as $error)
                  <p class="alert alert-danger">{{ $error }}</p>
              @endforeach

              @if (session('status'))
                  <div class="alert alert-success">
                      {{ session('status') }}
                  </div>
              @endif
              {!! Form::open(['class' => 'form']) !!}
              <div class="form-group">
                <div class="col-lg-12">
              {!! Form::text('name', $payer->name, [
                'class' => 'form-control',
                'id' => 'name',
                'placeholder' => trans('payers.title_name')
                ])
              !!}
              </div>
              </div>
              <div class="col-lg-12">
              <div class="form-group">
              {!! Form::submit(trans('main.update'), [
                'class' => 'btn btn-primary'
                ])
              !!}
              </div>
              </div>
              {!! Form::close() !!}

              {!! Form::open(['class' => 'form', 'method' => 'post', 'action' => ['Member\PayersController@destroy', $payer->id ]]) !!}
              <div class="col-lg-12">
              <div class="form-group">
              {!! Form::submit(trans('main.delete'), [
                'class' => 'btn btn-danger',
                ])
              !!}
              </div>
              </div>

              {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
