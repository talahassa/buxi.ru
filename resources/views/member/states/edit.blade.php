@extends('layouts.app')
@section('title', trans('states.edit'))

@section('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/css/bootstrap-colorpicker.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="container col-md-8 col-md-offset-2">
        <div class="card mt-5">
            <div class="card-header ">
                <h5 class="float-left mb-0">{{ trans('states.edit') }}</h5>
            </div>
            <div class="card-body mt-2">
              @foreach ($errors->all() as $error)
                  <p class="alert alert-danger">{{ $error }}</p>
              @endforeach

              @if (session('status'))
                  <div class="alert alert-success">
                      {{ session('status') }}
                  </div>
              @endif
              {!! Form::open(['class' => 'form']) !!}
              <div class="form-group">
                <div class="col-lg-12">
              {!! Form::text('name', $state->name, [
                'class' => 'form-control',
                'id' => 'name',
                'placeholder' => trans('states.title_name')
                ])
              !!}
              </div>
              </div>
              <div class="form-group">
                <div class="col-lg-12">
              {!! Form::text('color', $state->color, [
                'class' => 'form-control colorpicker',
                'id' => 'color',
                'placeholder' => trans('states.color')
                ])
              !!}
              </div>
              </div>
              <div class="col-lg-12">
              <div class="form-group">
              {!! Form::submit(trans('main.update'), [
                'class' => 'btn btn-primary'
                ])
              !!}
              </div>
              </div>

              {!! Form::close() !!}

              {!! Form::open(['class' => 'form', 'method' => 'post', 'action' => ['Member\StatesController@destroy', $state->id ]]) !!}
              <div class="col-lg-12">
              <div class="form-group">
              {!! Form::submit(trans('main.delete'), [
                'class' => 'btn btn-danger',
                ])
              !!}
              </div>
              </div>

              {!! Form::close() !!}
              @if($state->trashed())
              {!! Form::open(['class' => 'form', 'method' => 'post', 'action' => ['Member\StatesController@restore', $state->id ]]) !!}
              <div class="col-lg-12">
              <div class="form-group">
              {!! Form::submit(trans('main.restore'), [
                'class' => 'btn btn-success',
                ])
              !!}
              </div>
              </div>
              {!! Form::close() !!}
              @endif
            </div>
        </div>
    </div>
@endsection

@section('footer')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/js/bootstrap-colorpicker.min.js"></script>
<script>
  $('.colorpicker').colorpicker();
</script>
@endsection