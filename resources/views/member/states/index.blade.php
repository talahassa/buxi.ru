@extends('layouts.app')
@section('title', trans('states.title'))
@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-8 mt-2">
      <div class="card">
        <div class="card-header ">
          <h5 class="float-left m-0">{{ trans('states.title') }}</h5>
        </div>
        <div class="card-body mt-2">
          @if (session('status'))
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
          @endif
            @if ($states->isEmpty())
              <p>{{ trans('states.nodata') }}</p>
            @else
              <ul>
                @foreach($states as $state)
                  <li><a href="{{ action('Member\StatesController@edit', $state->id) }}">
                    @if($state->trashed()) <strike> @endif
                      <span style="background:{{ $state->color }};width: 15px;height: 15px;display:inline-block;"></span> {{ $state->name }}
                    @if($state->trashed()) </strike> @endif
                    </a></li>
                @endforeach
              </ul>
            @endif
        </div>
      </div>
    </div>
    <div class="col-md-4 mt-2">
        <div class="card">
            <div class="card-header ">
                <h5 class="float-left m-0">{{ trans('states.add') }}</h5>
            </div>
            <div class="card-body mt-2">
              @foreach ($errors->all() as $error)
                  <p class="alert alert-danger">{{ $error }}</p>
              @endforeach
              {!! Form::open(['class' => 'form', 'method' => 'post', 'action' => 'Member\StatesController@store']) !!}
              <div class="form-group">
                <div class="col-lg-12">
              {!! Form::text('name', null, [
                'class' => 'form-control',
                'id' => 'name',
                'placeholder' => trans('states.title_name')
                ])
              !!}
              </div>
              </div>
              <div class="form-group">
                <div class="col-lg-12">
              {!! Form::text('color', null, [
                'class' => 'form-control',
                'id' => 'color',
                'placeholder' => trans('states.color')
                ])
              !!}
              </div>
              </div>
              <div class="col-lg-12">
              <div class="form-group">
              {!! Form::submit(trans('states.add'), [
                'class' => 'btn btn-primary'
                ])
              !!}
              </div>
              </div>

              {!! Form::close() !!}
            </div>
        </div>
    </div>
  </div>
</div>
@endsection
