<nav class="navbar navbar-expand-md navbar-dark bg-dark shadow">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Buxi.ru') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          @if (Auth::check())
            @role('member')
            <ul class="navbar-nav mr-auto">     
                @if(Request::url() === env('APP_URL').'/member/transactions')
                  {{-- Modal Actions --}}
                  <li class="nav-item mr-1">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#incomeModal">
                      <i class="fas fa-plus"></i>
                    </button>
                  </li>
                  <li class="nav-item mr-1">
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#expenseModal">
                      <i class="fas fa-minus"></i>
                    </button>
                  </li>
                  <li class="nav-item mr-1">
                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#transferModal">
                      <i class="fas fa-exchange-alt"></i>
                    </button>
                  </li>
                @endif
                </ul>
                @yield('statistics')
                <ul class="navbar-nav ml-auto">                   
                  <li class="nav-item dropdown">
                    <a id="navbarSettings" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                      <i class="fas fa-toolbox"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right bg-dark" aria-labelledby="navbarSettings">
                      {!! link_to_route('accounts.index', trans('accounts.title'), null, array('class' => 'nav-link')); !!}
                      {!! link_to_route('payers.index', trans('payers.title'), null, array('class' => 'nav-link')); !!}
                      {!! link_to_route('states.index', trans('states.title'), null, array('class' => 'nav-link')); !!}
                      {!! link_to_route('types.index', trans('types.title'), null, array('class' => 'nav-link')); !!}                      
                    </div>
                  </li>
                  <li class="nav-item dropdown">
                    <a id="navbarReportDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                      <i class="fas fa-chart-line"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right bg-dark" aria-labelledby="navbarReportDropdown">
                      {!! link_to_route('reports.index', 'Все отчёты', null, array('class' => 'nav-link')); !!}
                      {!! link_to_route('reports.index', 'Создать отчёт', null, array('class' => 'nav-link')); !!}
                      {!! link_to_route('reports.thismonth', 'Текущий месяц', null, array('class' => 'nav-link')); !!}
                      {!! link_to_route('reports.previousmonth', 'Предыдущий месяц', null, array('class' => 'nav-link')); !!}
                      {!! link_to_route('reports.thisyear', 'Этот год', null, array('class' => 'nav-link')); !!}
                    </div>
                  </li>
                </ul>
              @endrole
            @endif
            <!-- Right Side Of Navbar -->
            <div><currency-component><currency-component></div>
            <ul class="navbar-nav ml-auto">              
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                          <img class="rounded-circle img-fluid" src="/storage/avatars/{{ Auth::user()->avatar }}" style="max-width: 20px;" /> 
                           {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                          <div class="text-center"><img class="rounded-circle img-fluid" src="/storage/avatars/{{ Auth::user()->avatar }}" style="max-width: 60px;" /> </div>
                        <a class="dropdown-item" href="{{action('Admin\UsersController@edit', ['id' => Auth::user()->id])}}">Профиль</a>
                          @role('admin')
                          <a class="dropdown-item" href="/admin/">Admin</a>
                          @endrole
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
