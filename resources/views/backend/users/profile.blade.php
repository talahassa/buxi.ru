@extends('layouts.app')
@section('title', 'Профиль пользователя {@ $user->name @}')
@section('content')

    <div class="container col-md-10 col-md-offset-2">
        <div class="card mt-5">
            <div class="card-header">
                <h5 class="float-left">Профиль пользователя  {{ $user->name }}</h5>
                <div class="clearfix"></div>
            </div>
            <div class="content">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                @if (count($errors) > 0))
                    <p> There is no user.</p>
                @else
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Photo</th>
                                <th>Name</th>
                                <th>Roles</th>
                                <th>Email</th>
                                <th>Joined at</th>

                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td><img class="rounded-circle" src="/storage/avatars/{{ $user->avatar }}" /></td>
                                    <td>
                                        <a href="{{ action('Admin\UsersController@edit', $user->id) }}">{{ $user->name }} </a>
                                    </td>
                                    <td>
                                      @foreach($user->roles as $role)
                                        {{ $role->name }}
                                      @endforeach
                                      </td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->created_at }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </div>

@endsection
