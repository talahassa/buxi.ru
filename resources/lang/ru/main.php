<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title' => 'Buxi.ru - самая классная экономическая стратегия, интегрированая в жизнь.',
    'update' => 'Обновить',
    'delete' => 'Удалить',
    'yes' => 'Да',
    'no' => 'Нет',
    'info' => 'Информация',
    'stats' => 'Статистика',
    'close' => 'Закрыть',
    'restore' => 'Восстановить',
    'income' => 'Доходы',
    'expence' => 'Расходы',
    'add' => 'Создать',
    'total' => 'Всего',

];
