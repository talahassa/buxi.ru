<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль должен содержать минимум шесть символов.',
    'reset' => 'Ваш пароль быть сброшен!',
    'sent' => 'Мы отправили Вам ссылку на сброс пароля!',
    'token' => 'Эта ссылка на сброс паролья больше не действительная.',
    'user' => "Пользователя с таким email не существует.",

];
