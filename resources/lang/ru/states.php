<?php

return [

    'title' => 'Статьи',
    'nodata' => 'Статей нет',
    'title_name' => 'Название',
    'add' => 'Добавить стаью',
    'edit' => 'Редактировать статью',
    'created' => 'Статья создана!',
    'color' => 'Цвет',

];
