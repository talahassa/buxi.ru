<?php

return [

    'title' => 'States',
    'nodata' => 'No states',
    'title_name' => 'Name',
    'add' => 'Add state',
    'edit' => 'Edit state',
    'created' => 'State saved!',
    'color' => 'Color',

];
