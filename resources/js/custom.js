
$(function () {
    $('body').on('click', function (e) {
        $('[data-toggle=popover]').each(function () {
            // hide any open popovers when the anywhere else in the body is clicked
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });
    
    $("[data-toggle=popover]").mousedown(function(){
      // toggle popover when link is clicked
      $(this).popover({
        html: true,
        sanitize: false,
      });
    });

  });