<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::get('/', 'WelcomeController@home');
Route::get('/', function () {
  if(Auth::check()) {
      return redirect('/member/transactions');
  } else {
      return view('auth.login');
  }
});



Route::group(array('prefix' => 'member', 'namespace' => 'Member', 'middleware' => 'auth'), function () {

  // VUE.js study
  Route::view('/vue', 'member.vue.index');
  Route::get('/api/account/{id}', 'API\TransactionsApiController@getAccountId');
  Route::resource('/api/transactions', 'API\TransactionsApiController');
  

  Route::get('/', 'TransactionsController@index');
  // Reports
  Route::get('/reports/thismonth','ReportsController@thisMonth')->name('reports.thismonth');
  Route::get('/reports/thisyear','ReportsController@thisYear')->name('reports.thisyear');
  Route::get('/reports/previousmonth','ReportsController@previousMonth')->name('reports.previousmonth');
  Route::get('/reports', 'ReportsController@index')->name('reports.index');
  Route::post('/reports', 'ReportsController@store');
  Route::get('/reports/{id}', 'ReportsController@show');
  Route::post('/reports/{id}/delete', 'ReportsController@destroy');

  // Transactions Reports Details
  Route::get('/transactions/filter', 'ReportsController@transactionsByFilter')->middleware('sanitize');

  // Accounts
  Route::get('/accounts', 'AccountsController@index')->name('accounts.index');
  Route::post('accounts', 'AccountsController@store');
  Route::get('/accounts/{id}', 'AccountsController@show');
  Route::get('/accounts/{id}/edit', 'AccountsController@edit');
  Route::post('/accounts/{id}/edit', 'AccountsController@update');
  Route::post('/accounts/{id}/delete','AccountsController@destroy');
  Route::post('/accounts/{id}/restore','AccountsController@restore');

  // Payers
  Route::get('/payers', 'PayersController@index')->name('payers.index');
  Route::get('/payers/{id}', 'PayersController@show');
  Route::post('/payers', 'PayersController@store');
  Route::get('/payers/{id}/edit', 'PayersController@edit');
  Route::post('/payers/{id}/edit', 'PayersController@update');
  Route::post('/payers/{id}/delete','PayersController@destroy');

  // States
  Route::get('/states', 'StatesController@index')->name('states.index');
  Route::post('/states', 'StatesController@store');
  Route::get('/states/{id}', 'StatesController@show');
  Route::get('/states/{id}/edit', 'StatesController@edit');
  Route::post('/states/{id}/edit', 'StatesController@update');
  Route::post('/states/{id}/delete','StatesController@destroy');
  Route::post('/states/{id}/restore','StatesController@restore');

  // Types
  Route::get('/types', 'TypesController@index')->name('types.index');
  Route::post('/types', 'TypesController@store');
  Route::get('/types/{id}', 'TypesController@show');
  Route::get('/types/{id}/edit', 'TypesController@edit');
  Route::post('/types/{id}/edit', 'TypesController@update');
  Route::post('/types/{id}/delete','TypesController@destroy');

  // Transactions
  Route::get('/transactions', 'TransactionsController@index')->name('transactions.index');
  Route::post('/transactions', 'TransactionsController@store')->name('transactions.store');
  Route::get('/transactions/{id}', 'TransactionsController@show');
  Route::get('/transactions/{id}/edit', 'TransactionsController@edit');
  Route::post('/transactions/{id}/edit', 'TransactionsController@update');
  Route::post('/transactions/{id}/delete','TransactionsController@destroy')->name('transactions.delete');

});

Route::group(array('prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'manager'), function () {
  Route::get('users', [ 'as' => 'admin.user.index', 'uses' => 'UsersController@index']);
    Route::get('users/{id?}/edit', 'UsersController@edit');
    Route::get('users/create', 'UsersController@create');
    Route::post('users/create', 'UsersController@store');
    Route::post('users/{id?}/edit','UsersController@update');
    Route::get('roles', 'RolesController@index');
    Route::get('roles/create', 'RolesController@create');
    Route::post('roles/create', 'RolesController@store');
    Route::get('/', 'AdminPageController@home');

    Route::get('users/{id?}/profile', 'UsersController@profile');
});

Auth::routes(['register' => false]);
