<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TicketsTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testBasicTest(){
      $response = $this->get('/');
      $response->assertStatus(200);
      $response->assertSeeText('Laravel');
    }
    
    public function testSeeText(){
      $response = $this->get('/');
      $response->assertSeeText('Laravel');
    }
}
