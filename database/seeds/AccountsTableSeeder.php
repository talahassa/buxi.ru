<?php

use Illuminate\Database\Seeder;
use App\Account;

class AccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('accounts')->truncate();

      Account::create([
        'name' => 'Сберкарта Дя'
      ]);

      Account::create([
        'name' => 'Сберкарта Н.'
      ]);

      Account::create([
        'name' => 'Депозит'
      ]);

      Account::create([
        'name' => 'WMZ'
      ]);

      Account::create([
        'name' => 'WMR'
      ]);

      Account::create([
        'name' => 'WME'
      ]);

      Account::create([
        'name' => 'Альфа руб.'
      ]);

      Account::create([
        'name' => 'Альфа доллар'
      ]);

      Account::create([
        'name' => 'Налик'
      ]);

      Account::create([
        'name' => 'Payoneer'
      ]);

      Account::create([
        'name' => 'Тиньков руб.'
      ]);

      Account::create([
        'name' => 'Тиньков доллар.'
      ]);

      Account::create([
        'name' => 'Тиньков евро.'
      ]);

      Account::create([
        'name' => 'PayPal'
      ]);

      Account::create([
        'name' => 'Сбер МСК'
      ]);

      Account::create([
        'name' => 'Yandex Money'
      ]);
    }
}
