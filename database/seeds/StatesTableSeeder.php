<?php

use Illuminate\Database\Seeder;
use App\State;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('states')->truncate();

      State::create([
        'name' => 'Работа',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Инвестиция',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Пища',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Здоровье',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Подарки',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Сервер',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Развлечения',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Бензин',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Автомобиль',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Табак',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Дом',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Транспорт',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Обучение',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Бытовая химия',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Коммуналка',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Спорт',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Вода',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Телефон',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Благотворительность',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Красота',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'ДР Наргизки',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'ДР Дядька',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Учеба',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Переезд',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Для дома',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Квартира',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Алкоголь',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Одежда',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Лекарства',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Обувь',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Английский',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Техника',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Интернет',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Пенсия',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Долг',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Отпуск',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Партнерские',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Сканирование',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Инвестирование',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Донорство',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Банк',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Налоги',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Кидс',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Штрафы',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Инструменты',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Визы',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Сад',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Огород',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Книги',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'ЗП рабочим',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Ипотека',
        'color' => '#fff'
      ]);

      State::create([
        'name' => 'Хобби',
        'color' => '#fff'
      ]);
    }
}
