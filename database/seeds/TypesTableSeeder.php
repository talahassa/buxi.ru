<?php

use Illuminate\Database\Seeder;
use App\Type;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('types')->truncate();

      Type::create([
        'id' => 1,
        'name' => 'Расход'
      ]);

      Type::create([
        'id' => 2,
        'name' => 'Доход'
      ]);

      Type::create([
        'id' => 3,
        'name' => 'Перевод'
      ]);
    }
}
