<?php

use Illuminate\Database\Seeder;
use App\Payer;

class PayersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('payers')->truncate();

      Payer::create([
        'name' => 'Дядя'
      ]);

      Payer::create([
        'name' => 'Наргиза'
      ]);

      Payer::create([
        'name' => 'Оба'
      ]);
    }
}
